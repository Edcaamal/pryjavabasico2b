/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasico2b;

/**
 *
 * @author edgar
 */
public class JavaBasico2B {
    
    static void encabezado(){
       System.out.println("  _    _         _____   ______ _____");     
       System.out.println(" | |  | |  /\\   / ____| |  ____|_   _|");     
       System.out.println(" | |  | | /  \\ | |      | |__    | |");     
       System.out.println(" | |  | |/ /\\ \\| |      |  __|   | |");     
       System.out.println(" | |__| / ____ \\ |____  | |     _| |_");     
       System.out.println("  \\____/_/    \\_\\_____| |_|    |_____|");  
        System.out.println("");
        separador(false);
       System.out.println("            EL CORONAVIRUS");
        separador(false);
    }

    static void cuerpo(){
        System.out.println("   1.- Reglas");
        System.out.println("   2.- Como no aburrirse en clase");
        System.out.println("   3.- Como hacer apuntes");
        System.out.println("   4.- Como hacer que el maestro de Java no les duerma");
        System.out.println("   5.- Como hacer que no te pare la policia");
    }

    static void separador(boolean tipo){
        if (tipo){
            System.out.println("========================================");
        }else{
            System.out.println("----------------------------------------");
        }
    }

    static void menucontroller(int optmenu){
        separador (false);
        if (optmenu == 1){
            System.out.println("- Es una enfermedad de tipo respiratorio");
        }
        if (optmenu == 2){
            System.out.println("- Fiebre");
            System.out.println("- Problemas respiratoriios");
            System.out.println("- Gripas");
        }
        
    }

     public static void main(String[] args) {
        // TODO code application logic hereSystem.o
        encabezado();
        // cuerpo();
        menucontroller(2);
        separador(true);
    }
    
}
